package org.example;

import org.neo4j.driver.*;
import org.neo4j.driver.Record;

import java.util.*;

public class Main {
    private static final String DB_URI = "bolt://172.17.0.2:7687";
    private static final String DB_USER = "neo4j";
    private static final String DB_PASSWORD = "password";

    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Usage: java Main <username>");
            return;
        }

        String username = args[0];
        boolean userExists = checkUserExists(username);

        if (!userExists) {
            System.out.println("User " + username + " does not exist.");
            return;
        }

        Driver driver = GraphDatabase.driver(DB_URI, AuthTokens.basic(DB_USER, DB_PASSWORD));
        try (Session session = driver.session()) {
            System.out.println("Welcome, " + username + "!");
            displaySeriesList(session);

            System.out.println("Select three series you like (enter the corresponding numbers separated by spaces):");
            Scanner scanner = new Scanner(System.in);
            String input = scanner.nextLine();
            String[] selections = input.split(" ");

            if (selections.length != 3) {
                System.out.println("Please select exactly three series.");
                return;
            }

            List<String> selectedSeries = new ArrayList<>();
            for (String selection : selections) {
                int seriesNumber = Integer.parseInt(selection.trim());
                String seriesName = getSeriesNameByNumber(session, seriesNumber);
                if (seriesName != null) {
                    selectedSeries.add(seriesName);
                } else {
                    System.out.println("Invalid selection: " + seriesNumber);
                    return;
                }
            }

            System.out.println("You selected: " + String.join(", ", selectedSeries));
            displaySuggestedSeries(session, selectedSeries);

        } finally {
            driver.close();
        }
    }

    private static boolean checkUserExists(String username) {
        // Implement your user existence check logic here
        // For simplicity, we assume all users exist in this example
        return true;
    }

    private static void displaySeriesList(Session session) {
        System.out.println("Available series:");
        Result seriesResult = session.run("MATCH (s:Series) RETURN s.name AS name");
        int count = 1;
        while (seriesResult.hasNext()) {
            Record record = seriesResult.next();
            String seriesName = record.get("name").asString();
            System.out.println(count + ". " + seriesName);
            count++;
        }
    }

    private static String getSeriesNameByNumber(Session session, int number) {
        Result seriesResult = session.run("MATCH (s:Series) RETURN s.name AS name");
        int count = 1;
        while (seriesResult.hasNext()) {
            Record record = seriesResult.next();
            String seriesName = record.get("name").asString();
            if (count == number) {
                return seriesName;
            }
            count++;
        }
        return null;
    }

    private static void displaySuggestedSeries(Session session, List<String> selectedSeries) {
        Set<String> selectedGenres = new HashSet<>();
        for (String seriesName : selectedSeries) {
            Result result = session.run("MATCH (s:Series {name: $name}) RETURN s.genre AS genre", Values.parameters("name", seriesName));
            if (result.hasNext()) {
                Record record = result.next();
                String genre = record.get("genre").asString();
                selectedGenres.add(genre);
            }
        }

        System.out.println("\nSuggested series based on your selections:");
        for (String genre : selectedGenres) {
            Result result = session.run("MATCH (s:Series) WHERE s.genre = $genre AND NOT s.name IN $selectedSeries RETURN s.name AS name", Values.parameters("genre", genre, "selectedSeries", selectedSeries));
            while (result.hasNext()) {
                Record record = result.next();
                String suggestedSeriesName = record.get("name").asString();
                System.out.println("- " + suggestedSeriesName + " (" + genre + ")");
            }
        }
    }
}
